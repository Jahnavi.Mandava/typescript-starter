const push = (arr: ReadonlyArray<number>, e: number): ReadonlyArray<number> => [
  ...arr,
  e
];
const splitAt = (
  arr: ReadonlyArray<number>,
  n: number
): [ReadonlyArray<number>, ReadonlyArray<number>] => {
  const result1: ReadonlyArray<number> = [];
  const result2: ReadonlyArray<number> = [];
  for (let i = 0; i < n; i += 1) {
    result1 = push(result1, arr[i]);
  }
  for (let i = n; i < arr.length; i += 1) {
    result2 = push(result2, arr[i]);
  }
  return [result1, result2];
};
export default splitAt;

const isLeap = (n: number): boolean => {
  return n % 400 === 0 || (n % 4 === 0 && n % 100 !== 0);
};
export default isLeap;

import power2 from './power';
test('power2', () => {
  expect(power2(2, 2)).toEqual(4);
  expect(power2(2, 3)).toEqual(8);
  expect(power2(3, 2)).toEqual(9);
  expect(power2(3, 3)).toEqual(27);
  expect(power2(3, 4)).toEqual(81);
});

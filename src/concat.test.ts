import concat from './concat';
test('concat', () => {
  expect(concat([1, 3, 5], [2, 4, 6])).toEqual([1, 3, 5, 2, 4, 6]);
  expect(concat([5, 7, 9], [2, 4, 6])).toEqual([5, 7, 9, 2, 4, 6]);
  expect(concat([5, 7, 9], [1, 3, 5])).toEqual([5, 7, 9, 1, 3, 5]);
  expect(concat([1, 2, 3], [4, 5, 6])).toEqual([1, 2, 3, 4, 5, 6]);
  expect(concat([5, 7, 9], [2, 6, 8])).toEqual([5, 7, 9, 2, 6, 8]);
});

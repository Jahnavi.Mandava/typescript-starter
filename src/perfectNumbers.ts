const range = (start: number, stop: number): number[] => {
  const arr: number[] = [];
  for (let i = start; i <= stop; i++) {
    arr.push(i);
  }
  return arr;
};

const perfect = (n: number): boolean => {
  let result = 0;
  for (let i = 1; i <= n / 2; i++) {
    if (n % i === 0) {
      result += i;
    }
  }

  if (result === n && result !== 0) {
    return true;
  } else {
    return false;
  }
};

const perfectNumbers = (start: number, stop: number): ReadonlyArray<number> =>
  range(start, stop).filter(perfect);
export { range, perfect, perfectNumbers };

import indexOf from './indexof';
test('indexOf', () => {
  expect(indexOf([2, 4, 5, 6], 6)).toEqual(3);
  expect(indexOf([2, 4, 5, 6], 2)).toEqual(0);
  expect(indexOf([2, 4, 5, 6], 4)).toEqual(1);
  expect(indexOf([2, 4, 5, 6], 5)).toEqual(2);
  expect(indexOf([2, 4, 5, 6], 1)).toEqual(-1);
});

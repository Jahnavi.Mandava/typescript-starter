const maxnumber = (arr: ReadonlyArray<number>): number =>
  arr.reduce((x, y) => (x > y ? x : y));
export default maxnumber;

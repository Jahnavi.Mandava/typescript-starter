const push = (arr: ReadonlyArray<number>, e: number): ReadonlyArray<number> => [
  ...arr,
  e
];
const concat2 = (
  arr: ReadonlyArray<number>,
  arr2: ReadonlyArray<number>
): ReadonlyArray<number> => {
  for (let i = 0; i < arr2.length; ++i) {
    arr = push(arr, arr2[i]);
  }
  return arr;
};
export default concat2;

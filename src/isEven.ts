const isEven = (n: number): boolean => {
  return n % 2 === 0;
};
const isOdd = (n: number): boolean => {
  return !isEven(n, 2);
};
export { isEven, isOdd };

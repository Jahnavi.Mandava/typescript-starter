class Complex {
  real: number;
  imaginary: number;
  constructor(real: number, imaginary: number) {
    this.real = real;
    this.imaginary = imaginary;
  }
  add(x: Complex): string {
    const real = this.real + x.real;
    const imaginary = this.imaginary + x.imaginary;
    return '' + real + '+' + imaginary + 'i';
  }
  subtract(x: Complex): string {
    const real = this.real - x.real;
    const imaginary = this.imaginary - x.imaginary;
    return '' + real + '-' + imaginary + 'i';
  }
  multiply(x: Complex): string {
    const real = this.real * x.real;
    const imaginary = this.imaginary * x.imaginary;
    return '' + real + '*' + imaginary + 'i';
  }
  divide(x: Complex): string {
    const real = this.real / x.real;
    const imaginary = this.imaginary / x.imaginary;
    return '' + real + '+' + imaginary + 'i';
  }
  compareto = (t: Complex): number => {
    const re = this.real + t.real;
    if (re < 0) {
      return -1;
    }
    if (re > 0) {
      return 1;
    } else {
      return 0;
    }
  };
}
const c1 = new Complex(1, 2);
const c2 = new Complex(3, 4);
const c3 = c1.add(c2);
const c4 = c1.subtract(c2);
const c5 = c1.multiply(c2);
const c6 = c1.divide(c2);
const c7 = c1.compareto(c2);

console.log(c3);
console.log(c4);
console.log(c5);
console.log(c6);
console.log(c7);

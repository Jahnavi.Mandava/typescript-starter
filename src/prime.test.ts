import prime from './prime';
test('prime', () => {
  expect(prime(2)).toEqual(true);
  expect(prime(3)).toEqual(true);
  expect(prime(6)).toEqual(false);
  expect(prime(8)).toEqual(false);
  expect(prime(1)).toEqual(false);
});

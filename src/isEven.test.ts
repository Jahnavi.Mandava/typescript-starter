import { isEven, isOdd } from './isEven';
test('isEven', () => {
  expect(isEven(6)).toEqual(true);
  expect(isEven(5)).toEqual(false);
  expect(isEven(3)).toEqual(false);
  expect(isEven(8)).toEqual(true);
  expect(isEven(2)).toEqual(true);
});
test('isOdd', () => {
  expect(isOdd(5)).toEqual(true);
  expect(isOdd(4)).toEqual(false);
  expect(isOdd(8)).toEqual(false);
  expect(isOdd(3)).toEqual(true);
  expect(isOdd(1)).toEqual(true);
});

import { factorial, ncr } from './factorial';
test('factorial', () => {
  expect(factorial(0)).toEqual(1);
  expect(factorial(1)).toEqual(1);
  expect(factorial(2)).toEqual(2);
  expect(factorial(5)).toEqual(120);
  expect(factorial(4)).toEqual(24);
});
test('ncr', () => {
  expect(ncr(0, 0)).toEqual(1);
  expect(ncr(2, 1)).toEqual(2);
  expect(ncr(5, 2)).toEqual(10);
  expect(ncr(10, 2)).toEqual(45);
  expect(ncr(5, 3)).toEqual(10);
});

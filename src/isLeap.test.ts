import isLeap from './isLeap';
test('isLeap', () => {
  expect(isLeap(2016)).toEqual(true);
  expect(isLeap(2017)).toEqual(false);
  expect(isLeap(2015)).toEqual(false);
  expect(isLeap(2012)).toEqual(true);
  expect(isLeap(1998)).toEqual(false);
});

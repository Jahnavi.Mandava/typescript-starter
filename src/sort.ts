const sort = (arr: ReadonlyArray<number>): ReadonlyArray<number> => {
  for (let i = 0; i < arr.length; i++) {
    let j = i - 1;
    let temp = arr[i];
    while (j > -1 && arr[j] > temp) {
      arr[j + 1] = arr[j];
      j--;
    }
    arr[j + 1] = temp;
  }
  return arr;
};
export default sort;

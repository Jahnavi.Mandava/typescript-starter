import splitAt from './splitAt';
test('splitAt', () => {
  expect(splitAt([1, 2, 3, 4], 3)).toEqual([[1, 2, 3], [4]]);
  expect(splitAt([1, 2, 3, 4], 2)).toEqual([[1, 2], [3, 4]]);
  expect(splitAt([1, 2, 3, 4], 1)).toEqual([[1], [2, 3, 4]]);
  expect(splitAt([1, 2, 3, 4], 4)).toEqual([[1, 2, 3, 4], []]);
  expect(splitAt([1, 2, 3, 4, 5], 4)).toEqual([[1, 2, 3, 4], [5]]);
});

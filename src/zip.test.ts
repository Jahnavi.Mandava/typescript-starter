import zip from './zip';
test('zip', () => {
  expect(zip([1, 2, 3], [4, 5, 6])).toEqual([[1, 4], [2, 5], [3, 6]]);
  expect(zip([1, 3, 5], [2, 4, 6])).toEqual([[1, 2], [3, 4], [5, 6]]);
  expect(zip([1, 2, 3], [6, 7, 8])).toEqual([[1, 6], [2, 7], [3, 8]]);
  expect(zip([4, 5, 6], [7, 8, 9])).toEqual([[4, 7], [5, 8], [6, 9]]);
  expect(zip([4, 5, 6], [1, 2, 3])).toEqual([[4, 1], [5, 2], [6, 3]]);
});

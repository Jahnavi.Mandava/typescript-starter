const gcd = (a: number, b: number): number => {
  if (!b) {
    return a;
  }
  return gcd(b, a % b);
};

class Rational {
  readonly numerator: number;
  readonly denominator: number;
  constructor(numerator: number, denominator: number) {
    const g = gcd(numerator, denominator);
    this.numerator = numerator / g;
    this.denominator = denominator / g;
  }
  add(that: Rational): Rational {
    const numerator =
      this.numerator * that.denominator + this.denominator * that.numerator;
    const denominator = this.denominator * that.denominator;
    const result = new Rational(numerator, denominator);
    return result;
  }
  subtract(that: Rational): Rational {
    const numerator =
      this.numerator * that.denominator - this.denominator * that.numerator;
    const denominator = this.denominator * that.denominator;
    const result = new Rational(numerator, denominator);
    return result;
  }
  Multiply(that: Rational): Rational {
    const numerator =
      this.numerator * that.denominator * this.denominator * that.numerator;
    const denominator = this.denominator * that.denominator;
    const result = new Rational(numerator, denominator);
    return result;
  }
  divide(that: Rational): Rational {
    const numerator =
      this.numerator * that.denominator / this.denominator * that.numerator;
    const denominator = this.denominator * that.denominator;
    const result = new Rational(numerator, denominator);
    return result;
  }
  compareto = (t: Rational): number => {
    return this.numerator * t.denominator - this.denominator * t.numerator;
  };
  less = (t: Rational): boolean => {
    return this.compareto(t) < 0;
  };
  equal = (t: Rational): boolean => {
    return this.compareto(t) === 0;
  };
  greater = (t: Rational): boolean => {
    return this.compareto(t) > 0;
  };
}
const r1 = new Rational(12, 2);
const r2 = new Rational(16, 4);
const r3 = r1.add(r2);
const r4 = r1.subtract(r2);
const r5 = r1.Multiply(r2);
const r6 = r1.divide(r2);
const r7 = r1.compareto(r2);
console.log(r3.numerator, r3.denominator);
console.log(r4.numerator, r4.denominator);
console.log(r5.numerator, r5.denominator);
console.log(r6.numerator, r6.denominator);
console.log(r7);

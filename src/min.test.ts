import min from './min';
test('min', () => {
  expect(min(4, 5)).toEqual(4);
  expect(min(3, 6)).toEqual(3);
  expect(min(4, 1)).toEqual(1);
  expect(min(2, 5)).toEqual(2);
});

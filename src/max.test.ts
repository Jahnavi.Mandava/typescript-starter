import max from './max';
test('max', () => {
  expect(max([24, 25])).toEqual(25);
  expect(max([100, 200, 24, 25])).toEqual(200);
  expect(max([200, 100])).toEqual(200);
  expect(max([200, 100, 400])).toEqual(400);
});

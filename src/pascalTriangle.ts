const factorial = (n: number): number => {
  return n === 1 ? n : n * factorial(n - 1);
};

const ncr = (n: number, r: number): number =>
  factorial(n) / (factorial(r) * factorial(n - r));

const pascalline = (n: number): number[] => range(0, n + 1).map(r => ncr(n, r));

const pascalTriangle = (lines: number): ReadonlyArray<ReadonlyArray<number>> =>
  range(0, lines + 1).map(line => pascalline(line));
console.log(pascalTriangle(5));

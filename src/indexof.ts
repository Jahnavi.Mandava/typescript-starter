const indexOf = (arr: number[], value: number): number => {
  let n = arr.indexOf(value);
  return n;
};
export default indexOf;

import { primeNumbers } from './primeNumbers';
test('primeNumbers', () => {
  expect(primeNumbers(1, 7)).toEqual([2, 3, 5, 7]);
  expect(primeNumbers(2, 11)).toEqual([2, 3, 5, 7, 11]);
  expect(primeNumbers(11, 17)).toEqual([11, 13, 17]);
  expect(primeNumbers(13, 19)).toEqual([13, 17, 19]);
  expect(primeNumbers(17, 27)).toEqual([17, 19, 23]);
});

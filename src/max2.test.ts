import max2 from './max2';
test('max2', () => {
  expect(max2(1, 2)).toEqual(2);
  expect(max2(2, 1)).toEqual(2);
  expect(max2(3, 4)).toEqual(4);
  expect(max2(4, 6)).toEqual(6);
});

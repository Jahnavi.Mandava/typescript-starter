import sort from './sort';
test('sort', () => {
  expect(sort([1, 4, 6, 2, 3, 5])).toEqual([1, 2, 3, 4, 5, 6]);
  expect(sort([1, 4, 6, 2, 9, 8])).toEqual([1, 2, 4, 6, 8, 9]);
  expect(sort([10, 8, 2, 3, 6, 1])).toEqual([1, 2, 3, 6, 8, 10]);
  expect(sort([1, 3, 2, 5, 4, 6])).toEqual([1, 2, 3, 4, 5, 6]);
  expect(sort([1, 4, 6, 2, 8, 5])).toEqual([1, 2, 4, 5, 6, 8]);
});

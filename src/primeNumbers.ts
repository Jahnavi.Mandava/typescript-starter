const range = (start: number, stop: number): number[] => {
  const arr: number[] = [];
  for (let i = start; i <= stop; i++) {
    arr.push(i);
  }
  return arr;
};

const prime = (n: number): boolean => {
  if (n === 1) {
    return false;
  } else if (n === 2) {
    return true;
  } else {
    for (let x = 2; x < n; x++) {
      if (n % x === 0) {
        return false;
      }
    }
    return true;
  }
};

const primeNumbers = (start: number, stop: number): number[] =>
  range(start, stop).filter(prime);

export { range, prime, primeNumbers };

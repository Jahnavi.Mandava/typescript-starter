import unique from './unique';
test('unique', () => {
  expect(unique([1, 2, 2, 3, 3, 4, 4, 5])).toEqual([1, 2, 3, 4, 5]);
  expect(unique([1, 2, 2, 3, 3, 5, 6, 6])).toEqual([1, 2, 3, 5, 6]);
  expect(unique([1, 2, 2, 3, 4, 5])).toEqual([1, 2, 3, 4, 5]);
  expect(unique([1, 2, 2, 5, 6, 6])).toEqual([1, 2, 5, 6]);
  expect(unique([1, 2, 2, 3, 3])).toEqual([1, 2, 3]);
});

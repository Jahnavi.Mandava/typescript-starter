import concat2 from './concat2';
test('concat2', () => {
  expect(concat2([1, 2, 3], [4, 5, 6])).toEqual([1, 2, 3, 4, 5, 6]);
  expect(concat2([1, 2, 3], [6, 7, 8])).toEqual([1, 2, 3, 6, 7, 8]);
  expect(concat2([1, 2, 3], [8, 9, 10])).toEqual([1, 2, 3, 8, 9, 10]);
  expect(concat2([4, 5, 6], [7, 8, 9])).toEqual([4, 5, 6, 7, 8, 9]);
  expect(concat2([1, 2, 3], [11, 12, 13])).toEqual([1, 2, 3, 11, 12, 13]);
});

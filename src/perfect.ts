const perfect = (n: number): boolean => {
  let result = 0;
  for (let i = 1; i <= n / 2; i++) {
    if (n % i === 0) {
      result += i;
    }
  }

  if (result === n && result !== 0) {
    return true;
  } else {
    return false;
  }
};
export default perfect;

const isFactor = (n: number, i: number): number => {
  return n % i === 0;
};
export default isFactor;

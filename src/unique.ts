const push = (arr: ReadonlyArray<number>, e: number): ReadonlyArray<number> => [
  ...arr,
  e
];
const unique = (arr: ReadonlyArray<number>): ReadonlyArray<number> => {
  let result: ReadonlyArray<number> = [arr[0]];
  for (let i = 1; i < arr.length; i++) {
    if (arr[i] !== arr[i - 1]) {
      result = push(result, arr[i]);
    }
  }
  return result;
};
export default unique;

import takeWhile from './takeWhile';
test('takeWhile', () => {
  expect(takeWhile([2, 4, 5, 6], x => x % 2 === 0)).toEqual([2, 4]);
  expect(takeWhile([2, 5, 4, 6], x => x % 2 === 0)).toEqual([2]);
  expect(takeWhile([2, 4, 8, 6, 5], x => x % 2 === 0)).toEqual([2, 4, 8, 6]);
  expect(takeWhile([2, 3, 4, 6], x => x % 2 === 0)).toEqual([2]);
  expect(takeWhile([2, 4, 6, 5, 8], x => x % 2 === 0)).toEqual([2, 4, 6]);
});

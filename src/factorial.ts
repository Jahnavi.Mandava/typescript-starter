const factorial = (n: number): number => {
  let fact = 1;
  for (let i = 1; i <= n; i++) {
    fact = fact * i;
  }
  return fact;
};
const ncr = (n: number, r: number): number => {
  return factorial(n) / (factorial(r) * factorial(n - r));
};
export { factorial, ncr };

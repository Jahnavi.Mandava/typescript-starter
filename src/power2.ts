const power2 = (x: number, y: number): number => Math.pow(x, y);
export default power2;

import isFactor from './isFactor';
test('isFactor', () => {
  expect(isFactor(2, 2)).toEqual(true);
  expect(isFactor(4, 2)).toEqual(true);
  expect(isFactor(5, 2)).toEqual(false);
  expect(isFactor(8, 2)).toEqual(true);
  expect(isFactor(9, 2)).toEqual(false);
});

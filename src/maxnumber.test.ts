import maxnumber from './maxnumber';
test('maxnumber', () => {
  expect(maxnumber([1, 2])).toEqual(2);
  expect(maxnumber([1, 4])).toEqual(4);
  expect(maxnumber([5, 2])).toEqual(5);
  expect(maxnumber([8, 10])).toEqual(10);
  expect(maxnumber([1, 9])).toEqual(9);
});

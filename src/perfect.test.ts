import perfect from './perfect';
test('perfect', () => {
  expect(perfect(6)).toEqual(true);
  expect(perfect(5)).toEqual(false);
});

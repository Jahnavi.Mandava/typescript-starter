const max2 = (x: number, y: number): number => {
  return x > y ? x : y;
};
export default max2;

const squareAll = (arr: number[]): number[] => {
  const res: number[] = [];
  for (let i = 0; i < arr.length; ++i) {
    res.push(arr[i] * arr[i]);
  }
  return res;
};
export default squareAll;

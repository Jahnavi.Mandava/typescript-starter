const push = (arr: ReadonlyArray<number>, e: number): ReadonlyArray<number> => [
  ...arr,
  e
];
const zip = (
  arr: ReadonlyArray<number>,
  arr2: ReadonlyArray<number>
): ReadonlyArray<[number, number]> => {
  let result: ReadonlyArray<[number, number]> = [];
  for (let i = 0; i < arr.length; i++) {
    result = push(result, [arr[i], arr2[i]]);
  }
  return result;
};
export default zip;

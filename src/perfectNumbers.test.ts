import { perfectNumbers } from './perfectNumbers';
test('perfectNumbers', () => {
  expect(perfectNumbers(1, 7)).toEqual([6]);
  expect(perfectNumbers(1, 30)).toEqual([6, 28]);
});

import allEven from './allEven';
test('allEven', () => {
  expect(allEven([2, 3, 4, 5, 6])).toEqual([2, 4, 6]);
  expect(allEven([8, 9, 10, 11, 12])).toEqual([8, 10, 12]);
  expect(allEven([8, 4, 2, 1, 12])).toEqual([8, 4, 2, 12]);
  expect(allEven([2, 6, 8, 9])).toEqual([2, 6, 8]);
  expect(allEven([8, 22, 24, 27, 98])).toEqual([8, 22, 24, 98]);
});

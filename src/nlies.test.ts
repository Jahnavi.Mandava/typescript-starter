import nlies from './nlies';
test('nlies', () => {
  expect(nlies(4, 5, 6)).toEqual(true);
  expect(nlies(5, 6, 7)).toEqual(true);
  expect(nlies(6, 8, 7)).toEqual(false);
  expect(nlies(5, 8, 6)).toEqual(false);
  expect(nlies(6, 7, 8)).toEqual(true);
});

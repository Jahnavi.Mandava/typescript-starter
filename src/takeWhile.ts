const push = (arr: ReadonlyArray<number>, e: number): ReadonlyArray<number> => [
  ...arr,
  e
];
const takeWhile = (
  arr: ReadonlyArray<number>,
  f: (x: number) => boolean
): ReadonlyArray<number> => {
  const a: ReadonlyArray<number> = [];
  for (const i of arr) {
    if (f(i)) {
      a = push(a, i);
    } else {
      break;
    }
  }
  return a;
};
export default takeWhile;

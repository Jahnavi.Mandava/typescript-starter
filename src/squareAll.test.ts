import squareAll from './squareAll';
test('squareAll', () => {
  expect(squareAll([1, 2, 3])).toEqual([1, 4, 9]);
  expect(squareAll([4, 5, 6])).toEqual([16, 25, 36]);
});

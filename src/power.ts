const power = (x: number, y: number): number => {
  const result = Math.pow(x, y);
  return result;
};
export default power;

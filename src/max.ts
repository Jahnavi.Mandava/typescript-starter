const max = (arr: number[]): number => {
  let max2 = arr[0];
  for (let i = 0; i <= arr.length; ++i) {
    if (arr[i] > max2) {
      max2 = arr[i];
    }
  }
  return max2;
};
export default max;

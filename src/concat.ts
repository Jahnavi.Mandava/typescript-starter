const concat = (arr1: number[], arr2: number[]): number[] => {
  for (let i = 0; i < arr2.length; ++i) {
    arr1.push(arr2[i]);
  }
  return arr1;
};
export default concat;
